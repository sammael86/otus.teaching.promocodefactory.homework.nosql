﻿using MongoDB.Driver;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using Otus.Teaching.Pcf.Administration.DataAccess;
using Otus.Teaching.Pcf.Administration.DataAccess.Data;

namespace Otus.Teaching.Pcf.Administration.IntegrationTests.Data
{
    public class TestDbInitializer
        : IDbInitializer
    {
        private readonly IMongoDatabase _database;

        public TestDbInitializer(IDataContext context)
        {
            _database = context.Database;
        }
        
        public void InitializeDb()
        {
            _database.Client.DropDatabase(_database.DatabaseNamespace.DatabaseName);

            _database.GetCollection<Role>(nameof(Role)).InsertMany(FakeDataFactory.Roles);
            _database.GetCollection<Employee>(nameof(Employee)).InsertMany(FakeDataFactory.Employees);
        }

        public void CleanDb()
        {
            _database.Client.DropDatabase(_database.DatabaseNamespace.DatabaseName);
        }
    }
}