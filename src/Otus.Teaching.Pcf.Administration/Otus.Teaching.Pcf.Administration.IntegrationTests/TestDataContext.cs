﻿using MongoDB.Driver;
using Otus.Teaching.Pcf.Administration.DataAccess;

namespace Otus.Teaching.Pcf.Administration.IntegrationTests
{
    public class TestDataContext
        : IDataContext
    {
        public IMongoDatabase Database { get; set; }

        public TestDataContext()
        {
            var connectionString = "mongodb://user:password@localhost:27018";
            var client = new MongoClient(connectionString);
            Database = client.GetDatabase("promocode-factory-administration-test");
        }
    }
}