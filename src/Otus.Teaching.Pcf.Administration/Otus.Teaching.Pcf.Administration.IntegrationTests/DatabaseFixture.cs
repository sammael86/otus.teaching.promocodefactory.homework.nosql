﻿using System;
using Otus.Teaching.Pcf.Administration.IntegrationTests.Data;

namespace Otus.Teaching.Pcf.Administration.IntegrationTests
{
    public class DatabaseFixture: IDisposable
    {
        private readonly TestDbInitializer _testDbInitializer;
        
        public DatabaseFixture()
        {
            DbContext = new TestDataContext();

            _testDbInitializer= new TestDbInitializer(DbContext);
            _testDbInitializer.InitializeDb();
        }

        public void Dispose()
        {
            _testDbInitializer.CleanDb();
        }

        public TestDataContext DbContext { get; private set; }
    }
}