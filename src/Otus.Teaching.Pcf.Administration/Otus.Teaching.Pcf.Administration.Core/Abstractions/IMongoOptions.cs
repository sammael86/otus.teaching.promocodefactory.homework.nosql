namespace Otus.Teaching.Pcf.Administration.Core.Abstractions
{
    public interface IMongoOptions
    {
        string ConnectionString { get; set; }
        string DatabaseName { get; set; }
    }
}