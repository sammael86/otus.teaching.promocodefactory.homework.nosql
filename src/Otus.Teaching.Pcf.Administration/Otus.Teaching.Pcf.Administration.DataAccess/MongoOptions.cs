using Otus.Teaching.Pcf.Administration.Core.Abstractions;

namespace Otus.Teaching.Pcf.Administration.DataAccess
{
    public class MongoOptions:IMongoOptions
    {
        public string ConnectionString { get; set; }
        public string DatabaseName { get; set; }
    }
}