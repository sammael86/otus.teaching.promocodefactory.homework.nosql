using MongoDB.Driver;

namespace Otus.Teaching.Pcf.Administration.DataAccess
{
    public interface IDataContext
    {
        IMongoDatabase Database { get; set;  }
    }
}