﻿using MongoDB.Driver;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;

namespace Otus.Teaching.Pcf.Administration.DataAccess.Data
{
    public class DbInitializer
        : IDbInitializer
    {
        private readonly IMongoDatabase _database;

        public DbInitializer(IDataContext context)
        {
            _database = context.Database;
        }

        public void InitializeDb()
        {
            _database.Client.DropDatabase(_database.DatabaseNamespace.DatabaseName);

            _database.GetCollection<Role>(nameof(Role)).InsertMany(FakeDataFactory.Roles);
            _database.GetCollection<Employee>(nameof(Employee)).InsertMany(FakeDataFactory.Employees);
        }
    }
}